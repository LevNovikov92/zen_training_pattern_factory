package zen_training_java_factory_method.messages;

import java.util.ArrayList;

import zen_training_java_factory_method.IMessage;

public class InfoMessage implements IMessage {
	private String mFrom;
	private String mTo;
	private ArrayList<String> mHeaders = new ArrayList<>();
	private String mBody;
	private static final String PREFIX = "Information message:\n";

	public InfoMessage(String from, String to, String body) {
		mFrom = from;
		mTo = to;
		mBody = body;
	}
	
	public void setHeaders(ArrayList<String> headers) {
		mHeaders = headers;
	}
	
	@Override
	public String getFrom() {
		return mFrom;
	}

	@Override
	public String getTo() {
		return mTo;
	}

	@Override
	public ArrayList<String> getHeaders() {
		return mHeaders;
	}

	@Override
	public String getBody() {
		return PREFIX + mBody;
	}

}
