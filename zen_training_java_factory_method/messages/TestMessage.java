package zen_training_java_factory_method.messages;

import java.util.ArrayList;

import zen_training_java_factory_method.IMessage;

public class TestMessage implements IMessage {
	private String mFrom;
	private String mTo;
	private ArrayList<String> mHeaders = new ArrayList<>();

	public TestMessage(String from, String to) {
		mFrom = from;
		mTo = to;
	}
	
	public void setHeaders(ArrayList<String> headers) {
		mHeaders = headers;
	}
	
	@Override
	public String getFrom() {
		return mFrom;
	}

	@Override
	public String getTo() {
		return mTo;
	}

	@Override
	public ArrayList<String> getHeaders() {
		return mHeaders;
	}

	@Override
	public String getBody() {
		return "Test message";
	}

}
