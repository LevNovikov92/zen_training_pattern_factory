package zen_training_java_factory_method;

public interface IMessageFactory {
	public abstract IMessage createMessage(int type, String from, String to, String body);
}
