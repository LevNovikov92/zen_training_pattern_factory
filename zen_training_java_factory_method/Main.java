package zen_training_java_factory_method;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		FactoryBuilder type1Builder = new FactoryBuilder(FactoryBuilder.TYPE1);
		IMessageFactory factory1Type1 = type1Builder.createFactory();
		IMessage infoMessageType1 = factory1Type1.createMessage(
				Type1MessageFactory.INFO_MESSAGE, "me", "you", "message body");
		printMessage(infoMessageType1);
		
		FactoryBuilder type2Builder = new FactoryBuilder(FactoryBuilder.TYPE2);
		IMessageFactory factoryType2 = type2Builder.createFactory();
		IMessage msg1 = factoryType2.createMessage(Type2MessageFactory.TEST_MESSAGE, "user1", "user2", "");
		printMessage(msg1);
	}
	
	static void printMessage(IMessage message) {
		ArrayList<String> headers = message.getHeaders();
		p("HEADERS:");
		for(String header: headers) {
			p(header);
		}
		p("FROM: " + message.getFrom());
		p("TO: " + message.getTo());
		p("BODY: " + message.getBody());
		p("");
	}
	
	static void p(String line) {
		System.out.println(line);
	}

}
