package zen_training_java_factory_method;

import java.util.ArrayList;

public interface IMessage {
	public abstract String getFrom();
	public abstract String getTo();
	public abstract ArrayList<String> getHeaders();
	public abstract String getBody();
}
