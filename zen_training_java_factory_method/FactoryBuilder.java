package zen_training_java_factory_method;

import java.lang.reflect.Constructor;

public class FactoryBuilder {
	private Constructor factoryConstructor;
	
	public static final String TYPE1 = "type1";
	public static final String TYPE2 = "type2";
	
	public FactoryBuilder(String type) {
		Class factoryClass = null;
		
		if(TYPE1.equals(type)) {
			factoryClass = Type1MessageFactory.class;
		}
		if(TYPE2.equals(type)) {
			factoryClass = Type2MessageFactory.class;
		}
		
		if(factoryClass != null) {
			try {
				factoryConstructor = factoryClass.getConstructor(null);
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}
		}
	}
	
	public IMessageFactory createFactory() {
		Object factory = null;
		Object[] args = {}; 
		try {
			factory = factoryConstructor.newInstance(args);
		} catch(Exception e) {
			System.out.println("Factory creating error");
		}
		return (IMessageFactory) factory; 
	}
}
