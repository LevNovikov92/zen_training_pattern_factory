package zen_training_java_factory_method;

import java.util.ArrayList;

import zen_training_java_factory_method.messages.InfoMessage;
import zen_training_java_factory_method.messages.TestMessage;

public class Type2MessageFactory implements IMessageFactory{
	public final static int INFO_MESSAGE = 0;
	public final static int TEST_MESSAGE = 1;
	private final static String HEADER1_HEADER = "Type2Header1";
	
	@Override
	public IMessage createMessage(int type, String from, String to, String body) {
		ArrayList<String> headers = new ArrayList<>();
		headers.add(HEADER1_HEADER);
		
		switch (type) {
		case INFO_MESSAGE:
			InfoMessage infoMessage = new InfoMessage(from, to, body);
			infoMessage.setHeaders(headers);
			return infoMessage;
			
		case TEST_MESSAGE:
			TestMessage testMessage = new TestMessage(from, to);
			testMessage.setHeaders(headers);
			return testMessage;

		default:
			return null;
		}
	}
}
